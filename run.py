import config
import anyio

from semaphore import Bot, ChatContext

from bot.dev import pve

# Connect the bot to number.
bot = Bot(config.PHONE_NUMBER)


@bot.handler("")
async def commandHandler(ctx: ChatContext) -> None:
    try:
        if ctx.message.source.number not in config.ALLOWED_SENDERS:
            await ctx.message.reply("Sender is not in the allowed senders list.")
            return None

        msg = ctx.message.get_body().split(" ")
        if msg[0] == "pve":
            await pve.handler(ctx, msg)
        elif msg[0] == "debug":
            await ctx.message.reply(f"{ctx}")
        else:
            return

    except Exception as e:
        await ctx.message.reply(f"An error has occured. {e}")


async def main():
    async with bot:
        # Set profile name.
        await bot.set_profile(config.BOT_NAME)

        # Run the bot until you press Ctrl-C.
        await bot.start()


if __name__ == "__main__":
    anyio.run(main)