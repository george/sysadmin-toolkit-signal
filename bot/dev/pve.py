import config
import os
import uuid
import datetime

from time import sleep
from typing import Optional
from math import trunc

from anyio import run_sync_in_worker_thread
from proxmoxer import ProxmoxAPI

proxmox = ProxmoxAPI(**config.PVE_INFO)


async def handler(ctx, args):
    arg_len = len(args)

    if arg_len == 1:
        await ctx.message.reply(
            "This is supposed to be a help message, but I'm too lazy to implement one at the moment."
        )
    if arg_len == 2:
        if args[1] == "get":
            pass

    elif arg_len == 3:
        if args[1] == "get":
            vmid = int(args[2])

            vm = await run_sync_in_worker_thread(get_vm_or_ct, vmid)

            if vm is None:
                await ctx.message.reply(
                    f"There was no VM or container found with id {vmid}"
                )
                return

            rrd_graphs, clean_rrd_file_paths = await run_sync_in_worker_thread(
                get_rrd_graphs,
                vmid,
                vm["node"],
                vm["type"],
                ["cpu", "mem", "netin, netout"],
                "day",
            )

            await ctx.message.reply(
                body=f"""
Here's the status of {vm['type']} {vmid} ({vm['name']}).

Uptime: {datetime.timedelta(seconds=vm['uptime'])}
Allocated vCPUs: {vm['cpu']}
Current RAM Usage: {vm['memory']['current']}/{vm['memory']['total']} MB ({vm['memory']['percent']}) 
{"Total " if vm['type'] == "VM" else ""}Disk: {f"{vm['disk']['current']}/{vm['disk']['total']} MB ({vm['disk']['percent']})" if vm['type'] == "CT" else f"{vm['disk']['total']} MB"}

Attached, you will find RRD graphs for CPU, RAM, and network statuses for the past day.
            """,
                attachments=rrd_graphs,
            )

            await run_sync_in_worker_thread(clean_up_rrd_graphs, clean_rrd_file_paths)
    elif arg_len == 4:
        pass
    elif arg_len == 5:
        pass


def get_vm_or_ct(vmid) -> Optional[dict]:
    """Searches through all the available Proxmox nodes to find a QEMU Virtual Machine or LXC Container with the specified ID."""
    for node in proxmox.nodes.get():
        for ct in proxmox.nodes(node["node"]).lxc.get():
            if int(ct["vmid"]) == vmid:
                return {
                    "node": node["node"],
                    "type": "CT",
                    "name": ct["name"],
                    "uptime": ct["uptime"],
                    "cpu": f"{ct['cpus']}",
                    "memory": {
                        "current": f"{trunc(int(ct['mem']) / 2**20)}",
                        "total": f"{trunc(int(ct['maxmem']) / 2**20)}",
                        "percent": f"{trunc((int(ct['mem']) * 100) / int(ct['maxmem']))}%",
                    },
                    "uptime": ct["uptime"],
                    "disk": {
                        "current": f"{trunc(int(ct['disk']) / 2**20)}",
                        "total": f"{trunc(int(ct['maxdisk']) / 2**20)}",
                        "percent": f"{trunc((int(ct['disk']) * 100) / int(ct['maxdisk']))}%",
                    },
                }

        for vm in proxmox.nodes(node["node"]).qemu.get():
            if int(vm["vmid"]) == vmid:
                vm = proxmox.nodes(node["node"]).qemu(int(vmid)).status.current.get()
                return {
                    "node": node["node"],
                    "type": "VM",
                    "name": vm["name"],
                    "uptime": vm["uptime"],
                    "cpu": f"{vm['cpus']}",
                    "memory": {
                        "current": f"{trunc(int(vm['mem']) / 2**20)}",
                        "total": f"{trunc(int(vm['maxmem']) / 2**20)}",
                        "percent": f"{trunc((int(vm['mem']) * 100) / int(vm['maxmem']))}%",
                    },
                    "uptime": vm["uptime"],
                    "disk": {
                        "total": f"{trunc(int(vm['maxdisk']) / 2**20)}",
                    },
                }


def get_rrd_graphs(vmid, node, type, sources=["cpu"], timeframe="day") -> list:
    rrd_graphs_attachment_list = []
    file_paths = []

    if timeframe not in ["minute", "hour", "day", "month", "year"]:
        raise Exception("Timeframe supplied not in allowed timeframes.")

    uniq = uuid.uuid4()
    for source in sources:
        file_name = f"/tmp/{uniq}_{source}.png"

        if source in ["cpu", "mem"]:
            source += f", max{source}"

        if type == "CT":
            response = (
                proxmox.nodes(node).lxc(vmid).rrd.get(ds=source, timeframe=timeframe)
            )
        elif type == "VM":
            response = (
                proxmox.nodes(node).qemu(vmid).rrd.get(ds=source, timeframe=timeframe)
            )

        with open(file_name, "wb") as f:
            f.write(response["image"].encode("raw_unicode_escape"))

        file_paths.append(file_name)
        rrd_graphs_attachment_list.append(
            {
                "filename": file_name,
                "width": "800",
                "height": "800",
            }
        )

    return rrd_graphs_attachment_list, file_paths


def clean_up_rrd_graphs(file_paths) -> None:
    sleep(20)
    for path in file_paths:
        os.remove(path)


# TODO: Clean up and refactor the following code. It is not used as of the time of writing, but was used during debugging.


def get_lxc_cts():
    containers = []

    for node in proxmox.nodes.get():
        for ct in proxmox.nodes(node["node"]).lxc.get():
            containers.append(
                "{0}, {1}. {2} => {3}\n".format(
                    node["node"], ct["vmid"], ct["name"], ct["status"]
                )
            )

    return "".join(sorted(containers))


def get_qemu_vms():
    vms = ""

    for node in proxmox.nodes.get():
        for vm in proxmox.nodes(node["node"]).qemu.get():
            vms += "{0}, {1}. {2} => {3}\n".format(
                node["node"], vm["vmid"], vm["name"], vm["status"]
            )

    return "".join(sorted(vms))


def change_status(node_id, vmid, type, status):
    if type == "CT":
        if status == "start":
            return proxmox.nodes(node_id).lxc(vmid).status.start.post()
        if status == "stop":
            return proxmox.nodes(node_id).lxc(vmid).status.stop.post()
        if status == "shutdown":
            return proxmox.nodes(node_id).lxc(vmid).status.shutdown.post()
        if status == "reboot":
            return proxmox.nodes(node_id).lxc(vmid).status.reboot.post()

    if type == "VM":
        if status == "start":
            return proxmox.nodes(node_id).qemu(vmid).status.start.post()
        if status == "stop":
            return proxmox.nodes(node_id).qemu(vmid).status.stop.post()
        if status == "shutdown":
            return proxmox.nodes(node_id).qemu(vmid).status.shutdown.post()
        if status == "reboot":
            return proxmox.nodes(node_id).qemu(vmid).status.reboot.post()


def get_lxc_ct(node_id, container_id):
    return proxmox.nodes(node_id).lxc(container_id).status.current.get()
